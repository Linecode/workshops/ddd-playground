﻿// See https://aka.ms/new-console-template for more information

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MyVod.Common.BuildingBlocks.EventSourcing;
using Vod.DummyEventStore;

Console.WriteLine("Hello, World!");

using IHost host = Host.CreateDefaultBuilder(args)
    .ConfigureServices(services =>
    {
        services.AddDbContext<EventStoreContext>(options =>
        {
            options.UseSqlite("DataSource=app.db;Cache=Shared");
        });
        services.AddScoped<EventStore>();
    }).Build();

await Process(host.Services);
    
await host.RunAsync();

static async Task Process(IServiceProvider serviceProvider)
{
    using var scope = serviceProvider.CreateScope();

    var eventStore = scope.ServiceProvider.GetRequiredService<EventStore>();
    var context = scope.ServiceProvider.GetRequiredService<EventStoreContext>();
    
    await context.Database.EnsureDeletedAsync();
    await context.Database.EnsureCreatedAsync();

    var movie = Movie.Create("Test");
    
    await eventStore.Save(movie.Id, movie);
    
    var movie2 = await eventStore.Get<Movie>(movie.Id);
    
    Console.WriteLine(movie2.Name);
}

public class Movie : AggregateBase
{
    public string Name { get; private set; }

    public override void Apply(IDomainEvent @event)
    {
        When((dynamic)@event);
    }

    private void When(Events.MovieCreated @event)
    {
        Name = @event.Name;
        
        Version += 1;
    }

    private void When(Events.MovieCreatedV2 @event)
    {
        
    }

    public static Movie Create(string name)
    {
        var movie = new Movie
        {
            Id = Guid.NewGuid()
        };

        var @event = new Events.MovieCreated { Name = name };
        movie.Apply(@event);
        movie.AddUncommittedEvent(@event);

        return movie;
    }

    // public static Movie CreateV2(string title, string author)
    // {
    //     
    // }

    public class Events
    {
        public class MovieCreated : IDomainEvent
        {
            public string Name { get; init; }
        }

        public class MovieCreatedV2 : IDomainEvent
        {
            public string Title { get; init; }
            public string Author { get; init; }
        }
    }
}